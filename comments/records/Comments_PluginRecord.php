<?php
namespace Craft;

class Comments_PluginRecord extends BaseRecord
{
    // Public Methods
    // =========================================================================

    public function getTableName()
    {
        return 'comments_plugins';
    }

    public function defineRelations()
    {
        return array();
    }
    

    // Protected Methods
    // =========================================================================

    protected function defineAttributes()
    {
        return array(
            'class' => array(AttributeType::ClassName, 'required' => true, 'unique' => true),
            'version' => array('maxLength' => 15, 'column' => ColumnType::Varchar, 'required' => true),
            'enabled' => AttributeType::Bool,
            'settings' => AttributeType::Mixed,
            'installDate' => array(AttributeType::DateTime, 'required' => true),
        );
    }
}
