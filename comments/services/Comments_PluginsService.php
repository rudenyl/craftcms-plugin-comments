<?php
namespace Craft;

/**
 * Comments_PluginsService provides APIs for loading Comments plugins.
 */
class Comments_PluginsService extends BaseApplicationComponent
{
    protected $api;

    function __construct()
    {
        $this->api = new CommentsPluginHelper;
    }

    /**
     * Parse calls to api.
     */
    public function __call($method, $arguments)
    {
        if ($this->api)
        {
            if (method_exists($this->api, $method))
            {
                return call_user_func_array([$this->api, $method], $arguments);
            }
        }
    }
}