<?php
namespace Craft;

/**
 * PluginHelper provides APIs for managing Comments plugin.
 *
 * Heavily based on CraftCMS' PluginsService class.
 *
 */

class CommentsPluginHelper
{
    /**
     * Plugin components.
     *
     * @var array
     */
    private $_autoloadClasses = ['Service', 'Controller', 'Helper', 'Model', 'Record'];

    /**
     * Stores whether plugins have been loaded yet for this request.
     *
     * @var bool
     */
    private $_pluginsLoaded = false;

    /**
     * Stores whether plugins are in the middle of being loaded.
     *
     * @var bool
     */
    private $_enabledPlugins = array();

    /**
     * Stores all plugins, whether installed or not.
     *
     * @var array
     */
    private $_plugins = array();

    /**
     * Stores all plugins settings.
     *
     * @var array
     */
    private $_pluginSettings = array();

    /**
     * Stores all plugins, whether installed or not.
     *
     * @var array
     */
    private $_allPlugins;

    /**
     * 
     */
    function __construct()
    {
        // import base plugin
        Craft::import('plugins.comments.helpers.CommentsBasePlugin');
    }

    /**
     * Loads the enabled plugins.
     *
     * @return null
     */
    public function boot()
    {
        if (!$this->_pluginsLoaded)
        {
            $rows = craft()->db->createCommand()
                ->select('*')
                ->from('comments_plugins')
                ->where('enabled=1')
                ->queryAll();

            $names = array();

            foreach ($rows as $row)
            {
                $plugin = $this->_getPlugin($row['class']);

                if ($plugin)
                {
                    $this->_autoloadPluginClasses($plugin);

                    $row['settings'] = JsonHelper::decode($row['settings']);
                    $row['installDate'] = DateTime::createFromString($row['installDate']);

                    $lcPluginHandle = mb_strtolower($plugin->getClassHandle());
                    $this->_plugins[$lcPluginHandle] = $plugin;
                    $this->_enabledPlugins[$lcPluginHandle] = $plugin;
                    $names[] = $plugin->getName();

                    $plugin->setSettings($row['settings']);

                    $plugin->isInstalled = true;
                    $plugin->isEnabled = true;
                }
            }

            $this->_sortPlugins($names, $this->_enabledPlugins);

            foreach ($this->_enabledPlugins as $plugin)
            {
                $plugin->init();
            }

            $this->_pluginsLoaded = true;
        }
    }

    /**
     * Returns all the plugins.
     *
     * @return BasePlugin[] The plugins.
     */
    public function getPlugins($enabledOnly = true)
    {
        if ($enabledOnly)
        {
            return $this->_enabledPlugins;
        }

        if (!isset($this->_allPlugins))
        {
            $this->_allPlugins = array();

            $pluginsPath = $this->getPath();
            $pluginFolderContents = IOHelper::getFolderContents($pluginsPath, false);

            if ($pluginFolderContents)
            {
                foreach ($pluginFolderContents as $pluginFolderContent)
                {
                    if (!IOHelper::folderExists($pluginFolderContent))
                    {
                        continue;
                    }

                    $pluginFolderContent = IOHelper::normalizePathSeparators($pluginFolderContent);
                    $pluginFolderName = mb_strtolower(IOHelper::getFolderName($pluginFolderContent, false));
                    $pluginFilePath = IOHelper::getFolderContents($pluginFolderContent, false, ".*CommentsPlugin\.php");

                    if (!is_array($pluginFilePath) || count($pluginFilePath) < 1)
                    {
                        continue;
                    }

                    $pluginFileName = IOHelper::getFileName($pluginFilePath[0], false);

                    $handle = mb_substr($pluginFileName, 0, mb_strlen($pluginFileName) - 14);
                    $lcHandle = mb_strtolower($handle);

                    if ($lcHandle === $pluginFolderName && !isset($this->_allPlugins[$lcHandle]))
                    {
                        if ($plugin = $this->getPlugin($handle, false))
                        {
                            $this->_allPlugins[$lcHandle] = $plugin;
                            $names[] = $plugin->getName();
                        }
                    }
                }

                if (!empty($names))
                {
                    $this->_sortPlugins($names, $this->_allPlugins);
                }

            }
        }

        return $this->_allPlugins;
    }

    /**
     * Returns a plugin by its handle.
     *
     * @param string $handle The plugin’s handle.
     *
     * @return BasePlugin|null The plugin.
     */
    public function getPlugin($handle)
    {
        $lcPluginHandle = mb_strtolower($handle);

        if (!array_key_exists($lcPluginHandle, $this->_plugins))
        {
            $handle = $this->_getPluginHandleFromFileSystem($handle);

            if ($plugin = $this->_getPlugin($handle))
            {
                $plugin->isInstalled = (bool) craft()->db->createCommand()
                    ->select('count(id)')
                    ->from('comments_plugins')
                    ->where(array('class' => $plugin->getClassHandle()))
                    ->queryScalar();
            }

            $this->_plugins[$lcPluginHandle] = $plugin;
        }

        return $this->_plugins[$lcPluginHandle];
    }

    /**
     * Installs a plugin by its handle.
     *
     * @param string $handle The plugin’s handle.
     *
     * @throws Exception|\Exception
     * @return bool Whether the plugin was installed successfully.
     */
    public function installPlugin($handle)
    {
        $plugin = $this->getPlugin($handle, false);

        if (!$plugin)
        {
            throw new Exception(Craft::t('No plugin exists with the class “{class}”', array('class' => $handle)));
        }

        if ($plugin->isInstalled)
        {
            return true;
        }

        $lcPluginHandle = mb_strtolower($plugin->getClassHandle());

        if ($plugin->onBeforeInstall() !== false)
        {
            $transaction = craft()->db->getCurrentTransaction() === null ? craft()->db->beginTransaction() : null;
            try
            {
                craft()->db->createCommand()->insert('comments_plugins', array(
                    'class' => $plugin->getClassHandle(),
                    'version' => $plugin->getVersion(),
                    'enabled' => true,
                    'installDate' => DateTimeHelper::currentTimeForDb(),
                ));

                $plugin->isInstalled = true;
                $plugin->isEnabled = true;
                $this->_enabledPlugins[$lcPluginHandle] = $plugin;

                //$plugin->createTables();
                $this->_autoloadPluginClasses($plugin);

                if ($transaction !== null)
                {
                    $transaction->commit();
                }
            }
            catch (\Exception $e)
            {
                if ($transaction !== null)
                {
                    $transaction->rollback();
                }

                throw $e;
            }

            $plugin->onAfterInstall();

            return true;
        }
    }

    /**
     * Uninstalls a plugin by its handle.
     *
     * @param string $handle The plugin’s handle.
     *
     * @throws Exception|\Exception
     * @return bool Whether the plugin was uninstalled successfully.
     */
    public function uninstallPlugin($handle)
    {
        $plugin = $this->getPlugin($handle, false);

        if (!$plugin)
        {
            throw new Exception(Craft::t('No plugin exists with the class “{class}”', array('class' => $handle)));
        }

        if (!$plugin->isInstalled)
        {
            return true;
        }

        $lcPluginHandle = mb_strtolower($plugin->getClassHandle());

        if (!$plugin->isEnabled)
        {
            $plugin->isEnabled = true;
            $this->_enabledPlugins[$lcPluginHandle] = $plugin;
            $this->_autoloadPluginClasses($plugin);

            $pluginRow = craft()->db->createCommand()
                ->select('id')
                ->from('comments_plugins')
                ->where('class=:class', array('class' => $plugin->getClassHandle()))
                ->queryRow();

            $pluginId = $pluginRow['id'];
        }

        $transaction = craft()->db->getCurrentTransaction() === null ? craft()->db->beginTransaction() : null;
        try
        {
            $plugin->onBeforeUninstall();

            //$plugin->dropTables();

            craft()->db->createCommand()->delete('comments_plugins', array('class' => $handle));

            if ($transaction !== null)
            {
                $transaction->commit();
            }
        }
        catch (\Exception $e)
        {
            if ($transaction !== null)
            {
                $transaction->rollback();
            }

            throw $e;
        }

        $plugin->isEnabled = false;
        $plugin->isInstalled = false;
        unset($this->_plugins[$lcPluginHandle]);

        return true;
    }

    /**
     * Enables a plugin by its handle.
     *
     * @param string $handle The plugin’s handle.
     *
     * @throws Exception
     * @return bool Whether the plugin was enabled successfully.
     */
    public function enablePlugin($handle)
    {
        $plugin = $this->getPlugin($handle, false);

        if (!$plugin)
        {
            throw new Exception(Craft::t('No plugin exists with the class “{class}”', array('class' => $handle)));
        }

        if (!$plugin->isInstalled)
        {
            throw new Exception(Craft::t('“{plugin}” can’t be enabled because it isn’t installed yet.', array('plugin' => $plugin->getName())));
        }

        if ($plugin->isEnabled)
        {
            // Done!
            return true;
        }

        $lcPluginHandle = mb_strtolower($plugin->getClassHandle());

        craft()->db->createCommand()->update('comments_plugins',
            array('enabled' => 1),
            array('class' => $plugin->getClassHandle())
        );

        $plugin->isEnabled = true;
        $this->_enabledPlugins[$lcPluginHandle] = $plugin;

        return true;
    }

    /**
     * Disables a plugin by its handle.
     *
     * @param string $handle The plugin’s handle.
     *
     * @throws Exception
     * @return bool Whether the plugin was disabled successfully.
     */
    public function disablePlugin($handle)
    {
        $plugin = $this->getPlugin($handle, false);

        if (!$plugin)
        {
            throw new Exception(Craft::t('No plugin exists with the class “{class}”', array('class' => $handle)));
        }

        if (!$plugin->isInstalled)
        {
            throw new Exception(Craft::t('“{plugin}” can’t be disabled because it isn’t installed yet.', array('plugin' => $plugin->getName())));
        }

        if (!$plugin->isEnabled)
        {
            return true;
        }

        $lcPluginHandle = mb_strtolower($plugin->getClassHandle());

        craft()->db->createCommand()->update('comments_plugins',
            array('enabled' => 0),
            array('class' => $plugin->getClassHandle())
        );

        $plugin->isEnabled = false;
        unset($this->_enabledPlugins[$lcPluginHandle]);

        return true;
    }

    /**
     * Returns the plugin settings.
     *
     * @param string $pluginClass The plugin's class.
     *
     * @return array|null
     */
    public function getPluginSettings($pluginClass)
    {
        if ($plugin = $this->getPlugin($pluginClass))
        {
            return $plugin->getSettings();
        }

        return [];
    }

    /**
     * Saves new plugin settings.
     *
     * @param string $pluginClass The plugin's class.
     * @param array  $settings      The new plugin settings
     */
    public function savePluginSettings($pluginClass, $settings)
    {
        if ($plugin = $this->getPlugin($pluginClass))
        {
            $preppedSettings = $plugin->prepSettings($settings);

            $plugin->setSettings($preppedSettings);

            if ($plugin->getSettings()->validate())
            {
                $settings = JsonHelper::encode($plugin->getSettings()->getAttributes());

                $affectedRows = craft()->db->createCommand()
                    ->update('comments_plugins', 
                        ['settings' => $settings],
                        ['class' => $plugin->getClassHandle()]
                    );

                return (bool) $affectedRows;
            }
        }

        return false;
    }

    /**
     * Calls a method on all plugins that have it, and returns an array of the results, indexed by plugin handles.
     *
     * @param string $method     The name of the method.
     * @param array  $args       Any arguments that should be passed when calling the method on the plugins.
     * @param bool   $ignoreNull Whether plugins that have the method but return a null response should be ignored. Defaults to false.
     *
     * @return array An array of the plugins’ responses.
     */
    public function call($method, $args = array(), $ignoreNull = false)
    {
        $allResults = array();

        foreach ($this->getPlugins() as $plugin)
        {
            if (method_exists($plugin, $method))
            {
                $result = call_user_func_array(array($plugin, $method), $args);
            }

            if (isset($result) && (!$ignoreNull || $result !== null))
            {
                $allResults[$plugin->getClassHandle()] = $result;
                unset($result);
            }
        }

        return $allResults;
    }

    /**
     * Returns an array of class names found in a given plugin folder.
     *
     * @param BasePlugin $plugin         The plugin.
     * @param string     $classSubfolder The subfolder to search.
     * @param string     $classSuffix    The class suffix we’re looking for.
     *
     * @return array The class names.
     */
    public function getPluginClasses(BasePlugin $plugin, $classSubfolder, $classSuffix)
    {
        $classes = array();

        $pluginHandle = $plugin->getClassHandle();
        $pluginFolder = mb_strtolower($plugin->getClassHandle());
        $pluginFolderPath = $this->getPath().$pluginFolder.'/';
        $classSubfolderPath = $pluginFolderPath.$classSubfolder.'/';

        if (IOHelper::folderExists($classSubfolderPath))
        {
            $filter = $pluginHandle.'(_.+)?'.$classSuffix.'\.php$';
            $files = IOHelper::getFolderContents($classSubfolderPath, false, $filter);

            if ($files)
            {
                foreach ($files as $file)
                {
                    $class = IOHelper::getFileName($file, false);
                    $classes[] = $class;

                    Craft::import("plugins.comments.plugins.{$pluginFolder}.{$classSubfolder}.{$class}");
                }
            }
        }

        return $classes;
    }


    // Protected Methods
    // =========================================================================

    /**
     * Returns the Comments plugins folder path.
     *
     * @return string The Comments plugin path.
     */
    protected function getPath()
    {
        return craft()->path->getPluginsPath() . '/comments/plugins/';
    }


    // Private Methods
    // =========================================================================

    /**
     * Returns a new plugin instance based on its class handle.
     *
     * @param string $handle
     *
     * @return BasePlugin|null
     */
    private function _getPlugin($handle)
    {
        $class = $handle.'CommentsPlugin';
        $nsClass = __NAMESPACE__.'\\'.$class;

        if (!class_exists($nsClass, false))
        {
            $path = $this->getPath().mb_strtolower($handle).'/'.$class.'.php';

            if (($path = IOHelper::fileExists($path, false)) !== false)
            {
                require_once $path;
            }
            else
            {
                return null;
            }
        }


        if (!class_exists($nsClass, false))
        {
            return null;
        }

        $plugin = new $nsClass;

        if (!$plugin instanceof IPlugin)
        {
            return null;
        }

        return $plugin;
    }

    /**
     * Returns the actual plugin class handle based on a case-insensitive handle.
     *
     * @param string $iHandle
     *
     * @return bool|string
     */
    private function _getPluginHandleFromFileSystem($iHandle)
    {
        $pluginsPath = $this->getPath();
        $fullPath = $pluginsPath.mb_strtolower($iHandle).'/'.$iHandle.'CommentsPlugin.php';

        if (($file = IOHelper::fileExists($fullPath, true)) !== false)
        {
            $file = IOHelper::getFileName($file, false);

            return mb_substr($file, 0, mb_strlen($file) - mb_strlen('CommentsPlugin'));
        }

        return false;
    }

    /**
     * Finds and imports all of the auto-loadable classes for a given plugin.
     *
     * @param BasePlugin $plugin
     *
     * @return null
     */
    private function _autoloadPluginClasses(BasePlugin $plugin)
    {
        foreach ($this->_autoloadClasses as $classSuffix)
        {
            $classSubfolder = mb_strtolower($classSuffix).'s';
            $this->getPluginClasses($plugin, $classSubfolder, $classSuffix);
        }
    }

    /**
     * @param $names
     * @param $secondaryArray
     *
     * @return null
     */
    private function _sortPlugins(&$names, &$secondaryArray)
    {
        if (PHP_VERSION_ID < 50400)
        {
            array_multisort($names, $secondaryArray);
        }
        else
        {
            array_multisort($names, SORT_NATURAL | SORT_FLAG_CASE, $secondaryArray);
        }
    }
}