<?php
namespace Craft;

/**
 * Provides APIs for rendering Comments plugin templates.
 *
 * Extends CraftCMS' TemplatesService class and overwriting specific methods as required.
 */
class CommentsTemplateService extends TemplatesService
{
    /**
     * @var
     */
    private $_twig;

    function __construct()
    {
        $this->_twig = $this->getTwig(__NAMESPACE__.'\\CommentsTemplateLoader');
    }

    /**
     * Renders a template.
     *
     * @param mixed $template  The name of the template to load, or a StringTemplate object.
     * @param array $variables The variables that should be available to the template.
     *
     * @return string The rendered template.
     */
    public function render($template, $variables = array())
    {
        return $this->_twig->render($template, $variables);
    }

    /**
     * Finds a template on the file system and returns its path.
     *
     * All of the following files will be searched for, in this order:
     *
     *
     * @param string $name The name of the template.
     *
     * @return string|false The path to the template if it exists, or `false`.
     */
    public function findTemplate($name)
    {
        if ($loader = $this->_twig->getLoader())
        {
            return $loader->findTemplate($name);
        }

        return false;
    }
}