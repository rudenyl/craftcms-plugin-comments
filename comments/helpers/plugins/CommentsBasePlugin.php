<?php
namespace Craft;

/**
 * Base plugin class for Comments.
 *
 * Extends CraftCMS' BasePlugin class and overwriting specific methods as required.
 */

class CommentsBasePlugin extends BasePlugin
{
    /**
     * Require interface methods.
     */
    public function getVersion() {}
    public function getDeveloper() {}
    public function getDeveloperUrl() {}

    /**
     * Returns the record classes provided by this plugin.
     *
     * @param string|null $scenario The scenario to initialize the records with.
     *
     * @return BaseRecord[]
     */
    public function getRecords($scenario = null)
    {
        $records = array();
        $classes = craft()->plugins->getPluginClasses($this, 'records', 'Record', false);

        foreach ($classes as $class)
        {
            if (craft()->components->validateClass($class))
            {
                $class = __NAMESPACE__.'\\'.$class;
                $records[] = new $class($scenario);
            }
        }

        return $records;
    }

    /**
     * Returns the Comments plugin handle, ideally based on the class name.
     *
     * @return string The Comments plugin’s handle.
     */
    public function getClassHandle()
    {
        $classHandle = mb_substr(get_class($this), mb_strlen(__NAMESPACE__) + 1);

        $handle = mb_substr($classHandle, 0, mb_strlen($classHandle) - 14);

        return $handle;
    }
}