<?php
namespace Craft;

/**
 * Loads Comments plugin templates into Twig.
 *
 * Extends CraftCMS' TemplateLoader class and overwriting specific methods as required.
 */

class CommentsTemplateLoader extends TemplateLoader
{
    /**
     * Gets the source code of a template.
     *
     * @param  string $name The name of the template to load, or a StringTemplate object.
     *
     * @throws Exception
     * @return string The template source code.
     */
    public function getSource($name)
    {
        if (is_string($name))
        {
            $template = $this->_findTemplate($name);

            if (IOHelper::isReadable($template))
            {
                return IOHelper::getFileContents($template);
            }
            else
            {
                throw new Exception(Craft::t('Tried to read the template at {path}, but could not. Check the permissions.', array('path' => $template)));
            }
        }
        else
        {
            return $name->template;
        }
    }

    /**
     * Gets the cache key to use for the cache for a given template.
     *
     * @param string $name The name of the template to load, or a StringTemplate object.
     *
     * @return string The cache key (the path to the template)
     */
    public function getCacheKey($name)
    {
        if (is_string($name))
        {
            return $this->_findTemplate($name);
        }
        else
        {
            return $name->cacheKey;
        }
    }

    /**
     * Returns whether the cached template is still up-to-date with the latest template.
     *
     * @param string $name The template name, or a StringTemplate object.
     * @param int    $time The last modification time of the cached template
     *
     * @return bool
     */
    public function isFresh($name, $time)
    {
        if (craft()->request->isCpRequest() && craft()->updates->isCraftDbMigrationNeeded())
        {
            return false;
        }

        if (is_string($name))
        {
            $sourceModifiedTime = IOHelper::getLastTimeModified($this->_findTemplate($name));
            return $sourceModifiedTime->getTimestamp() <= $time;
        }
        else
        {
            return false;
        }
    }

    /**
     * Finds a template on the file system and returns its path.
     *
     * @param string $name The name of the template.
     *
     * @return string|false The path to the template if it exists, or `false`.
     */
    public function findTemplate($name)
    {
        $name = trim(preg_replace('#/{2,}#', '/', strtr($name, '\\', '/')), '/');

        $this->_validateTemplateName($name);

        $_defaultTemplatePath = craft()->path->getTemplatesPath();

        $basePaths = array($_defaultTemplatePath);

        if (craft()->request->isSiteRequest() && IOHelper::folderExists($this->_templatesPath.craft()->language))
        {
            $basePaths[] = $_defaultTemplatePath.craft()->language.'/';
        }

        foreach ($basePaths as $basePath)
        {
            if (($path = $this->_templateExists($basePath, $name)) !== null)
            {
                return $path;
            }
        }

        if (craft()->request->isCpRequest() || craft()->request->isActionRequest())
        {
            $name = craft()->request->decodePathInfo($name);

            $parts = array_filter(explode('/', $name));
            $pluginHandle = StringHelper::toLowerCase(array_shift($parts));

            $basePath = craft()->path->getPluginsPath().'comments/plugins/'.StringHelper::toLowerCase($pluginHandle).'/templates/';

            $tempName = implode('/', $parts);

            if (($path = $this->_templateExists($basePath, $tempName)) !== null)
            {
                return $path;
            }

            // attempt to fetch from available modules
            if (!empty($parts) && $parts[0] == 'modules')
            {
                array_shift($parts);
                $moduleHandle = StringHelper::toLowerCase(array_shift($parts));

                $basePath = craft()->path->getPluginsPath().'comments/plugins/'.StringHelper::toLowerCase($pluginHandle)
                    .'/modules/'.$moduleHandle.'/templates/';

                $tempName = implode('/', $parts);

                if (($path = $this->_templateExists($basePath, $tempName)) !== null)
                {
                    return $path;
                }
            }
        }

        return false;
    }

    // Private Methods
    // =========================================================================

    /**
     * Returns the path to a given template, or throws a TemplateLoaderException.
     *
     * @param $name
     *
     * @throws TemplateLoaderException
     * @return string $name
     */
    private function _findTemplate($name)
    {
        $template = $this->findTemplate($name);

        if (!$template)
        {
            throw new TemplateLoaderException($name);
        }

        return $template;
    }

    /**
     * Searches for a template files, and returns the first match if there is one.
     *
     * @param string $basePath The base path to be looking in.
     * @param string $name     The name of the template to be looking for.
     *
     * @return string|null The matching file path, or `null`.
     */
    private function _templateExists($basePath, $name)
    {
        $basePath = rtrim(IOHelper::normalizePathSeparators($basePath), '/').'/';
        $name = trim(IOHelper::normalizePathSeparators($name), '/');

        $_indexTemplateFilenames = ['index'];
        $_defaultTemplateExtensions = ['html', 'twig'];

        if ($name)
        {
            $testPath = $basePath.$name;

            if (IOHelper::fileExists($testPath))
            {
                return $testPath;
            }

            foreach ($_defaultTemplateExtensions as $extension)
            {
                $testPath = $basePath.$name.'.'.$extension;

                if (IOHelper::fileExists($testPath))
                {
                    return $testPath;
                }
            }
        }

        foreach ($_indexTemplateFilenames as $filename)
        {
            foreach ($_defaultTemplateExtensions as $extension)
            {
                $testPath = $basePath.($name ? $name.'/' : '').$filename.'.'.$extension;

                if (IOHelper::fileExists($testPath))
                {
                    return $testPath;
                }
            }
        }
    }

    /**
     * Ensures that a template name isn't null, and that it doesn't lead outside the template folder. 
     * Borrowed from Twig_Loader_Filesystem.
     *
     * @param string $name
     *
     * @throws \Twig_Error_Loader
     */
    private function _validateTemplateName($name)
    {
        if (mb_strpos($name, "\0") !== false)
        {
            throw new \Twig_Error_Loader(Craft::t('A template name cannot contain NUL bytes.'));
        }

        if (PathHelper::ensurePathIsContained($name) === false)
        {
            throw new \Twig_Error_Loader(Craft::t('Looks like you try to load a template outside the template folder: {template}.', array('template' => $name)));
        }
    }
}