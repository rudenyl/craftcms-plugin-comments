<?php
namespace Craft;

/**
 * Base controller class for Comments plugins.
 *
 * Extends CraftCMS' BaseController class and overwriting specific methods as required.
 */
class CommentsPluginController extends BaseController
{
    protected $templates;

    function __construct()
    {
        $this->templates = new CommentsTemplateService;
    }

    /**
     * Renders a template, and either outputs or returns it.
     *
     * @param mixed $template      The name of the template to load.
     * @param array $variables     The variables that should be available to the template.
     * @param bool  $return        Whether to return the results, rather than output them. (Default is `false`.)
     * @param bool  $processOutput Whether the output should be processed by processOutput() of the parent class.
     *
     * @throws HttpException
     * @return mixed The rendered template if $return is set to `true`.
     */
    public function renderTemplate($template, $variables = array(), $return = false, $processOutput = false)
    {
        if (($output = $this->templates->render($template, $variables)) !== false)
        {
            if ($processOutput)
            {
                $output = $this->processOutput($output);
            }

            if ($return)
            {
                return $output;
            }
            else
            {
                if (!HeaderHelper::isHeaderSet('Content-Type'))
                {
                    $templateFile = $this->templates->findTemplate($template);
                    $extension = IOHelper::getExtension($templateFile, 'html');

                    if ($extension == 'twig')
                    {
                        $extension = 'html';
                    }

                    HeaderHelper::setContentTypeByExtension($extension);
                }

                HeaderHelper::setHeader(array('charset' => 'utf-8'));

                if (in_array(HeaderHelper::getMimeType(), array('text/html', 'application/xhtml+xml')))
                {
                    $headHtml = craft()->templates->getHeadHtml();
                    $footHtml = craft()->templates->getFootHtml();

                    if ($headHtml)
                    {
                        if (($endHeadPos = mb_stripos($output, '</head>')) !== false)
                        {
                            $output = mb_substr($output, 0, $endHeadPos).$headHtml.mb_substr($output, $endHeadPos);
                        }
                        else
                        {
                            $output .= $headHtml;
                        }
                    }

                    if ($footHtml)
                    {
                        if (($endBodyPos = mb_stripos($output, '</body>')) !== false)
                        {
                            $output = mb_substr($output, 0, $endBodyPos).$footHtml.mb_substr($output, $endBodyPos);
                        }
                        else
                        {
                            $output .= $footHtml;
                        }
                    }
                }

                ob_start();
                echo $output;

                // End the request
                craft()->end();
            }
        }
        else
        {
            throw new HttpException(404);
        }
    }
}