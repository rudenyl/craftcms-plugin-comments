# Comments

Based on Josh Crawford's Comments plugin for the CraftCMS.

Visit [Comments](https://github.com/engram-design/Comments) Github repository for more plugin details.


## Sample Usage

On the block section of your frontend template (news entry, etc.), insert the following.

```
{% block content %}
    <link rel="stylesheet" href="/path/to/comments.css">

    <div class="news_wrap clearfix">
        <h1>{{ entry.title }}</h1>
        <div class="post-date">Posted on {{ entry.postDate.format('F d, Y') }}</div>

    	<article>
    		{{ entry.body }}
    	</article>
    </div>

    {{ craft.comments.form(entry.id, []) }}

    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script>
    $(function() {
        $(document)
            .on('click', 'a.comment-toggle', function(e) {
                e.preventDefault();
                $(this).parents('.comment-single:first').find('.comments-list:first').toggle();
            })
            .on('click', 'a.comment-reply', function(e) {
                e.preventDefault();

                $(this).closest('li.comment-single').find('.comment-form').first().toggle();
            })
            .on('click', 'a.comment-edit', function(e) {
                e.preventDefault();

                // Simply hides text and shows form
                $(this).parents('.comment-text').find('.comment-content').hide();
                $(this).parents('.comment-text').find('.edit-comment-form').show();
            });
    });
    </script>    
{% endblock %}
```

The javascript snippets, and other implementations, can also be found in the ```/examples``` folder.


## Documentation

Please visit the [Wiki](https://github.com/engram-design/Comments/wiki) for all documentation, a getting started guide, template tags, and developer resources.